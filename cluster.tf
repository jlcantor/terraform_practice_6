resource "aws_security_group" "allowweb" {
  name        = "allow_access_web"
  description = "Allow web access"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_launch_configuration" "web_conf" {
  name          = "web_config"
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  user_data = "${file("setupnginx.sh")}"

  security_groups = ["${aws_security_group.allowweb.name}"]

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "webcluster" {
  name                 = "web-cluster"
  launch_configuration = "${aws_launch_configuration.web_conf.name}"
  min_size             = 1
  max_size             = 2
  availability_zones   = ["us-west-1a"]

  load_balancers = ["${aws_elb.web.name}"]

  health_check_grace_period = 120

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_elb" "web" {
  name               = "web-elb"
  availability_zones = ["us-west-1a", "us-west-1b"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }


  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 5
  }

  tags {
    Name = "jose.cantor"
  }
}